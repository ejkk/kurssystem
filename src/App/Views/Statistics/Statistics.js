import React from 'react';
import './statistics.css'

const Statistics = (props) => {
    return (
        <div className='statistics'>
            <div className='statistics-box'>
                <h5>Total kursinkomst</h5>
                <h2>{props.sData.cashMade} kr</h2>
            </div>
            <div className='statistics-box'>
                <h5>Kursdeltagare totalt</h5>
                <h2>{props.sData.totalParticipants} st</h2>
            </div>
            <div className='statistics-box'>
                <h5>Totalt antal kurser</h5>
                <h2>{props.aCourses.amountOfCourses} st</h2>
            </div>
        </div>
    );
}

export default Statistics