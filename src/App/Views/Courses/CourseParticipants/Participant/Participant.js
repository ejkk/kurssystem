import React, { Component } from 'react'
import './participant.css'
import { deleteParticipant } from '../../../../Redux/Actions/courses-actions'
import { connect } from 'react-redux'

class Participant extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dropdownIsDown: false
        }

        this.handleClick = this.handleClick.bind(this);
        this.deletePart = this.deletePart.bind(this);
    }

    handleClick(e) {
        if (e.target.tagName === 'BUTTON') { } else {
            if (e.target.className === 'part-info' || e.target.className === 'part'
                || e.target.className === 'hotel' || e.target.className === 'part-container') {
                this.setState({
                    dropdownIsDown: this.state.dropdownIsDown ? false : true
                });
            }
        }
    }

    deletePart(e) {
        var { partid, dispatch, courseid } = this.props;

        dispatch(deleteParticipant({
            courseid,
            partID: partid
        }));

        fetch('https://expressbacken.herokuapp.com/courses/participant/delete/' + this.props.partid + '')

        this.setState({
            dropdownIsDown: false
        })
    }

    render() {
        const style = {
            backgroundColor: this.state.dropdownIsDown ? '#F5F5F5' : ''
        }

        return (
            <div onClick={this.handleClick} className='participant'>
                <div className='part-container' style={style}>
                    <div className='part'>{this.props.partName}</div>
                    <div className='hotel'></div>
                </div>
                {this.state.dropdownIsDown === true ?
                    <div className='p-info'>
                        <div className='t-info'>
                            <div className='registered'>Anmäld</div>
                            <div className='hotel2'></div>
                        </div>
                        <div className='t-data'>
                            <div className='registered'>{this.props.registered}</div>
                            <div className='hotel2'></div>
                        </div>
                        {Date.parse(this.props.courseStartDate) - Date.parse(new Date()) > 0 ? <button className='unreg' onClick={this.deletePart}>Avanmäl deltagaren</button> : ''}
                    </div> : ''}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    coursesArr: state.courses
});

export default connect(mapStateToProps)(Participant);