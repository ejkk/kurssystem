import React, { Component } from 'react'
import './courseparticipants.css'
import Participant from './Participant/Participant'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import PostIt from '../../../Elements/PostIt/PostIt'
import { connect } from 'react-redux'
import ChangeTodo from '../../Todo/ChangeTodo_courses/ChangeTodo'


class CourseParticipants extends Component {
  constructor(props) {
    super(props)

    this.state = {
      popupIsOpen: false,
      changeTodoPopupIsOpen: false,
      todoToChangeData: this.props.arr[0]
    }

    this.handleChangePopup = this.handleChangePopup.bind(this)
  }

  handleClick() {
    this.setState({
      popupIsOpen: this.state.popupIsOpen ? false : true
    });
  }

  updateTodo(id, title) {
    fetch('https://expressbacken.herokuapp.com/todos/title/update/' + id +'/' + title + '')
  }

  handleChangePopup() {

    this.setState({
      changeTodoPopupIsOpen: this.state.changeTodoPopupIsOpen ? false : true
    });

  }


  render() {
    return (
      <div className='course-participants'>
        <div className='courseTodoContainer'> {this.props.arr.map(item => { return item.id === this.props.courseid ? this.state.changeTodoPopupIsOpen ? <ChangeTodo todoData={item} closeIt={this.handleChangePopup} /> : '' : ''}) }  {this.props.arr.map(item => {return item.id === this.props.courseid ? <PostIt handleChangePopup={this.handleChangePopup} todoID={item.id} postitID={item.id} todoTitle={item.title} todoItems={item.items} /> : ''}) }</div>
        <div className='mini-continer'>
          <FontAwesomeIcon icon={faTimes} className='close-popup fa-m' onClick={this.props.closePopup} />
          <h3>{this.props.course}</h3>
          <div className='mini-info'>
            {this.props.courseStarts + " - " + this.props.endTime}
            <br />
            {this.props.place}
            <br />
            Kostnad: {this.props.price} kr
              </div>
          <div className='t'>
            <div className='part'>
              Deltagare
                </div>
            <div className='hotel'>
            </div>
          </div>
          {this.props.participants.map((item, key) => { return <Participant courseStartDate={this.props.startDate} hasEnded={this.props.hasEnded} courseid={this.props.courseid} partid={item.id} partName={item.foreName + " " + item.sureName} registered={item.registered} /> })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  arr: state.todos

});

export default connect(mapStateToProps)(CourseParticipants);