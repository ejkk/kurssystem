import React, { Component } from 'react';
import './courses.css'
import TopItem from './TopItem/TopItem'
import AllCoursesItem from './AllCoursesItem/AllCoursesItem'
import CourseParticipants from './CourseParticipants/courseParticipants'
import NewCourse from './NewCourse/NewCourse'
import UpdateCourse from './UpdateCourse/Updateourse'
import { connect } from 'react-redux'
import { getAllCourses } from '../../Redux/Actions/courses-actions'

class Courses extends Component {
    constructor(props) {
        super(props);
        this.state = {
            courseParticipantsIsOpen: false,
            newCourseIsOpen: false,
            updateCourseIsOpen: false,
            clickedCourse: '',
            updateCourse: ''
        }

        this.handleCourseParticipantsPopup = this.handleCourseParticipantsPopup.bind(this);
        this.handleNewCoursePopup = this.handleNewCoursePopup.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.handleUpdateoursePopup = this.handleUpdateoursePopup.bind(this);
    }

    componentDidMount() {
        fetch('https://expressbacken.herokuapp.com/courses')
            .then(res => res.json())
            .then(item => {
                if (this.props.coursesArr.length === 0) {
                    this.props.dispatch(getAllCourses(item));
                }
            })
            .catch(err => '')
    }

    handleCourseParticipantsPopup(courseName) {
        this.setState({
            courseParticipantsIsOpen: this.state.courseParticipantsIsOpen ? false : true,
            clickedCourse: courseName
        });

    }

    handleNewCoursePopup() {
        this.setState({
            newCourseIsOpen: this.state.newCourseIsOpen ? false : true
        });
    }

    handleUpdateoursePopup(data) {
        this.setState({
            updateCourseIsOpen: this.state.updateCourseIsOpen ? false : true, updateCourse: data
        })
    }

    render() {
        return (
            <div className='courses' >
                <div className='top-container'>
                    <div className='course-f'>
                        <h3>5 kommande kurser</h3>
                        <div className='top-t'>
                            <div className='course'>
                                Kurs
                            </div>
                            <div className='starts'>
                                Startar om
                            </div>
                            <div className='btn'>
                            </div>
                        </div>
                        {this.props.coursesArr.map((item, index) => {
                            if (Date.parse(item.startDate) - Date.parse(new Date()) > 0) {
                                if (index < 5) {
                                    return <TopItem key={item.id} hasEnded={item.courseHasEnded} starDate={item.startDate} course={item.name} popup={this.handleCourseParticipantsPopup} />
                                }
                            }
                        })}

                    </div>
                    <div className='course-f'>
                        <h3>5 senaste avlutade kurserna</h3>
                        <div className='top-t'>
                            <div className='course'>
                                Kurs
                            </div>
                            <div className='starts'>
                                Avslutad
                            </div>
                            <div className='btn'>
                            </div>
                        </div>
                        {this.props.coursesArr.map((item, index) => {
                            if (Date.parse(item.startDate) - Date.parse(new Date()) < 0) {
                                if (index < 5) {
                                    return <TopItem key={item.id} hasEnded={item.courseHasEnded} starDate={item.startDate} course={item.name} popup={this.handleCourseParticipantsPopup} />
                                }
                            }
                        })}
                    </div>
                </div>
                <div className='all-courses'>
                    <h3>Alla kommande kurser <a onClick={this.handleNewCoursePopup}>Lägg till kurs</a></h3>
                    <div className='all-courses-top'>
                        <div className='a-course'>kurs</div>
                        <div className='a-date'>Datum</div>
                        <div className='a-part'>Deltagare</div>
                        <div className='a-btn'></div>
                    </div>
                    {this.props.coursesArr.map(item => {
                        return item.courseHasEnded === 0 ? <AllCoursesItem courseData={item} updateCoursePopup={this.handleUpdateoursePopup} key={item.id} course={item.name} courseDate={item.startDate} part={item.participants.length + "/" + item.maxParticipants} participants={item.participants} popup={this.handleCourseParticipantsPopup} /> : ''
                    })}
                </div>
                {this.state.courseParticipantsIsOpen ? this.props.coursesArr.map((item, key) => { if (item.name === this.state.clickedCourse) { return <CourseParticipants startDate={item.startDate} hasEnded={item.courseHasEnded} price={item.price} endTime={item.endTime} key={item.id} courseid={item.id} course={item.name} participants={item.participants} courseStarts={item.startDate + " " + item.time} place={item.place} closePopup={this.handleCourseParticipantsPopup} /> } }) : ''}
                {this.state.newCourseIsOpen ? <NewCourse closePopup={this.handleNewCoursePopup} /> : ''}
                {this.state.updateCourseIsOpen ? <UpdateCourse courseData={this.state.updateCourse} closePopup={this.handleUpdateoursePopup} /> : ''}
            </div >
        );
    }
}

const mapStateToProps = state => ({
    coursesArr: state.courses
});

export default connect(mapStateToProps)(Courses);