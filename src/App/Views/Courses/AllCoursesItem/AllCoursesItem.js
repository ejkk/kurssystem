import React, { Component } from 'react';
import './allcoursesitem.css'

export default class allcoursesitem extends Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
        this.updateCourse = this.updateCourse.bind(this);
    }

    handleClick(e) {
        if (e.target.tagName !== 'BUTTON') {
            this.props.popup(this.props.course);
        }
    }

    updateCourse() {
        this.props.updateCoursePopup(this.props.courseData)
    }

    render() {

        return (
            <div className='item' onClick={this.handleClick}>
                <div className='a-course'>{this.props.course}</div>
                <div className='a-date'>{this.props.courseDate}</div>
                <div className='a-part'>{this.props.part}</div>
                <div className="btn"><button onClick={this.updateCourse}>Ändra</button></div>
            </div>
        );
    }
}
