import React, { Component } from 'react';
import './topitem.css'

export default class TopItem extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    daysFromNow() {
        var date = new Date();
        var today = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();

        let dt1 = new Date(this.props.starDate);
        let dt2 = new Date(today);

        let test = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
        test = test.toString().replace('-', '');
        var d;
        var d2;
        if (Date.parse(this.props.startDate) - Date.parse(new Date('yyyy-mm-dd')) < 0) {
            d = ' dag' + this.props.sen;
            d2 = ' dagar' + this.props.sen;
        } else {
            d = ' dag' + this.props.sen;
            d2 = ' dagar' + this.props.sen;
        }
        if (test <= 1) {
            return test + d;
        } else {
            return test + d2;
        }
    }

    handleClick() {
        this.props.popup(this.props.course);
    }
    render() {
        return (
            <div className='item' onClick={this.handleClick}>
                <div className='course'>
                    {this.props.course}
                </div>
                <div className='starts'>
                    {this.daysFromNow()}
                </div>
                <div className="btn"></div>
            </div>
        )
    };
}

