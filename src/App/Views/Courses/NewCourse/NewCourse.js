import React, { Component } from 'react';
import './newcourse.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { newCourseNow } from '../../../Redux/Actions/combined-actions'

export default class NewCourse extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showErrorMsg: false
        }
        this.addCourse = this.addCourse.bind(this);
    }

    addCourse() {
        let courseName = document.getElementsByName('titel')[0].value;
        let startDate = document.getElementsByName('startDate')[0].value;
        let startTime = document.getElementsByName('startTime')[0].value;
        let endTime = document.getElementsByName('endTime')[0].value;
        let price = document.getElementsByName('price')[0].value;
        let place = document.getElementsByName('place')[0].value;
        let lunch = document.getElementsByName('lunch')[0].value;
        let maxPart = document.getElementsByName('maxPart')[0].value;
        let registrationCloses = document.getElementsByName('registrationCloses')[0].value;
        let info = document.getElementsByName('info')[0].value;
        lunch = lunch.toString()
        maxPart = maxPart.toString()

        if (courseName === '' || startDate === '' || startTime === '' || endTime === '' || price === '' || place === '' || maxPart === '' || registrationCloses === '' || info === '') {
            this.setState({
                showErrorMsg: true
            })
        } else {
            var courseID = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
            let test = {
                name: courseName,
                startDate: startDate,
                time: startTime,
                endTime: endTime,
                place: place,
                lunch: lunch,
                maxParticipants: maxPart,
                amountOfCurrentparticipants: 0,
                registrationEnds: registrationCloses,
                info: info,
                courseHasEnded: 0,
                id: courseID,
                price: price,
                participants: []
            }

            this.props.dispatch(newCourseNow(test));

            fetch('https://expressbacken.herokuapp.com/courses/new/' + courseName + '/' + startDate + '/' + startTime + '/' + endTime + '/' + price + '/' + place + '/' + maxPart + '/' + registrationCloses + '/' + info + '/' + courseID + '/' + lunch + '')
            this.props.closePopup();
        }
    }

    render() {
        return (
            <div className='new-course'>
                <div className='nwCourse-container'>
                    <div className='err-msg'>{this.state.showErrorMsg ? 'Inget fält får vara tomt. Försök igen' : ''}</div>
                    <FontAwesomeIcon icon={faTimes} className='close-popup fa-m' onClick={this.props.closePopup} />
                    <input type='text' placeholder='Titel' name='titel' required />
                    <div className='inpMini-container'>
                        <div className='date-input-title'>Kursen startar: </div>
                        <input type='date' placeholder='Kursen börjar' name='startDate' required />
                    </div>
                    <div className='inpMini-container'>
                        <div className='date-input-title'>Pågår mellan kl: </div>
                        <input type='time' placeholder='08:00' name='startTime' required />
                        <input type='time' placeholder='08:00' name='endTime' required />
                    </div>
                    <input type='number' placeholder='Pris(kr) tex: 1500' name='price' required />
                    <input type='text' placeholder='Plats' name='place' required />
                    <div className='date-input-title'>Ingår lunch? </div>
                    <select name="lunch" value={this.props.value} required>
                        <option disabled hidden value=''></option>
                        <option value={0}>Nej</option>
                        <option value={1}>Ja</option>
                    </select>
                    <input type='number' placeholder='Max deltagare' name='maxPart' required />
                    <div className='date-input-title'>Anmälan stänger: </div>
                    <input type='date' placeholder='Anmälan stänger' name='registrationCloses' required />
                    <textarea placeholder='Kursinfo' name='info' required></textarea>
                    <button onClick={this.addCourse}>Lägg till kursen</button>
                </div>
            </div>
        );
    }
}


