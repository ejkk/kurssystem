import React, { Component } from 'react';
import './updatecourse.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { updateCourse } from '../../../Redux/Actions/courses-actions'

export default class UpdateCourse extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: this.props.courseData.name,
            endTime: this.props.courseData.endTime,
            id: this.props.courseData.id,
            info: this.props.courseData.info,
            lunch: this.props.courseData.lunch,
            maxParticipants: this.props.courseData.maxParticipants,
            place: this.props.courseData.place,
            price: this.props.courseData.price,
            startDate: this.props.courseData.startDate,
            time: this.props.courseData.time,
            registrationEnds: this.props.courseData.registrationEnds,
            participants: this.props.participants,
            showErrorMsg: false

        }
        this.updateCourse = this.updateCourse.bind(this);
        this.changeName = this.changeName.bind(this);
    }

    updateCourse() {
        let courseName = document.getElementsByName('titel')[0].value;
        let startDate = document.getElementsByName('startDate')[0].value;
        let startTime = document.getElementsByName('startTime')[0].value;
        let endTime = document.getElementsByName('endTime')[0].value;
        let price = document.getElementsByName('price')[0].value;
        let place = document.getElementsByName('place')[0].value;
        let lunch = document.getElementsByName('lunch')[0].value;
        let maxPart = document.getElementsByName('maxPart')[0].value;
        let registrationCloses = document.getElementsByName('registrationCloses')[0].value;
        let info = document.getElementsByName('info')[0].value;
        lunch = lunch.toString()
        maxPart = maxPart.toString()

        let course = {
            name: courseName,
            startDate: startDate,
            time: startTime,
            endTime: endTime,
            place: place,
            lunch: lunch,
            maxParticipants: maxPart,
            amountOfCurrentparticipants: 0,
            registrationEnds: registrationCloses,
            info: info,
            courseHasEnded: 0,
            id: this.state.id,
            price: price,
            participants: []
        }

        if (courseName === '' || startDate === '' || startTime === '' || endTime === '' || price === '' || place === '' || maxPart === '' || registrationCloses === '' || info === '') {
            this.setState({
                showErrorMsg: true
            })
        } else {
            fetch('https://expressbacken.herokuapp.com/courses/update/' + courseName + '/' + startDate + '/' + startTime + '/' + endTime + '/' + price + '/' + place + '/' + maxPart + '/' + registrationCloses + '/' + info + '/' + this.state.id + '/' + lunch + '')

            this.setState({
                showErrorMsg: false
            })

            this.props.dispatch(updateCourse(course));
            this.props.closePopup();
        }
    }

    changeName(e) {
        this.setState({
            name: e.target.value
        })
    }

    changeStartDate(e) {
        this.setState({
            startDate: e.target.value
        })
    }

    changeStartTime(e) {
        this.setState({
            time: e.target.value
        })
    }

    changeEndTime(e) {
        this.setState({
            endTime: e.target.value
        })
    }

    changePrice(e) {
        this.setState({
            price: e.target.value
        })
    }

    changePlace(e) {
        this.setState({
            place: e.target.value
        })
    }

    changeMaxParticipants(e) {
        this.setState({
            maxParticipants: e.target.value
        })
    }

    changeRegistrationEnds(e) {
        this.setState({
            registrationEnds: e.target.value
        })
    }

    changeInfo(e) {
        this.setState({
            info: e.target.value
        })
    }

    render() {
        return (
            <div className='new-course'>
                <div className='nwCourse-container'>
                    <div className='err-msg'>{this.state.showErrorMsg ? 'Inget fält får vara tomt. Försök igen' : ''}</div>
                    <FontAwesomeIcon icon={faTimes} className='close-popup fa-m' onClick={this.props.closePopup} />
                    <input type='text' placeholder='Titel' name='titel' value={this.state.name} onChange={(e) => this.changeName(e)} required />
                    <div className='inpMini-container'>
                        <div className='date-input-title'>Kursen startar: </div>
                        <input type='date' placeholder='Kursen börjar' name='startDate' value={this.state.startDate} onChange={(e) => this.changeStartDate(e)} required />
                    </div>
                    <div className='inpMini-container'>
                        <div className='date-input-title'>Pågår mellan kl: </div>
                        <input type='time' placeholder='08:00' name='startTime' value={this.state.time} onChange={(e) => this.changeStartTime(e)} required />
                        <input type='time' placeholder='08:00' name='endTime' value={this.state.endTime} onChange={(e) => this.changeEndTime(e)} required />
                    </div>
                    <input type='number' placeholder='Pris(kr) tex: 1500' name='price' value={this.state.price} onChange={(e) => this.changePrice(e)} required />
                    <input type='text' placeholder='Plats' name='place' value={this.state.place} onChange={(e) => this.changePlace(e)} required />
                    <div className='date-input-title'>Ingår lunch? </div>
                    <select name="lunch" value={this.props.value} required>
                        <option disabled hidden value=''></option>
                        <option value={0} selected={this.state.lunch === '0' ? true : false}>Nej</option>
                        <option value={1} selected={this.state.lunch === '1' ? true : false}>Ja</option>
                    </select>
                    <input type='number' placeholder='Max deltagare' name='maxPart' value={this.state.maxParticipants} onChange={(e) => this.changeMaxParticipants(e)} required />
                    <div className='date-input-title'>Anmälan stänger: </div>
                    <input type='date' placeholder='Anmälan stänger' name='registrationCloses' value={this.state.registrationEnds} onChange={(e) => this.changeRegistrationEnds(e)} required />
                    <textarea placeholder='Kursinfo' name='info' value={this.state.info} onChange={(e) => this.changeInfo(e)} required></textarea>
                    <button onClick={this.updateCourse}>Spara</button>
                </div>
            </div>
        );
    }
}


