import React, { Component } from 'react'
import './todopopup.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux'
import { newTodo } from '../../../Redux/Actions/todos-actions'


class TodoPopup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputs: []
        };

        this.newItem = this.newItem.bind(this);
    }

    newItem() {
        var joined = this.state.inputs.concat('s' + this.state.inputs.length + 1);
        this.setState({ inputs: joined })
    }

    remove(id) {
        let element = document.getElementById(id);
        element.parentElement.innerHTML = '';
    }

    handleSubmit() {
        var title = document.getElementsByName("title")[0].value;
        var items = document.getElementsByName("todItem");
        let i = 0;
        let item_values = [];
        for (i; i < items.length; i++) {
            item_values.push(items[i].value);
        }
        var randomID = Math.random().toString(36).substring(2, 14) + Math.random().toString(36).substring(2, 14);
        var newTodos = {
            title,
            items: item_values.map(item => {
                return { data: item, stroke: false, id: Math.random().toString(36).substring(2, 14) + Math.random().toString(36).substring(2, 14), todos_id: randomID }
            }),
            id: randomID
        }

        // didnt work with a method for the random ids for some reason
        this.props.dispatch(newTodo(newTodos));

        for (let i = 0; i < newTodos.items.length; i++) {
            fetch('https://expressbacken.herokuapp.com/todos/items/new/' + newTodos.items[i].data + '/' + newTodos.id + '/' + newTodos.items[i].id + '')
        }

        fetch('https://expressbacken.herokuapp.com/todos/new/' + title + '/' + newTodos.id + '').catch(err => console.log(err))
    }

    style = {
        fontSize: '20px',
        marginBottom: '15px'
    }

    render() {
        let { inputs } = this.state;
        return (
            <div className='popup'>
                <div className='new-todo' id='test'>
                    <FontAwesomeIcon icon={faTimes} className='close-popup fa-m' onClick={this.props.closeIt} />
                    <div className='cont'>
                        <input type='text' name='title' placeholder='Titel' style={this.style} />
                        <input type='text' name='todItem' placeholder='Att göra' />
                        {inputs.map((key, index) => <div className='small-container' key={key + 1} ><input name='todItem' id={'input-' + index} type='text' placeholder='Att göra' key={key + 2} className='newInput' /><FontAwesomeIcon key={key + 3} icon={faTimes} className='icon fa-xs' onClick={(() => this.remove('input-' + index, this))} /></div>)}
                    </div>
                    <div className='btns'>
                        <button onClick={this.newItem}>Lägg till rad</button>
                        <button onClick={() => {
                            this.props.closeIt(); this.handleSubmit();
                        }}>Spar</button>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    todosArr: state
});


export default connect(mapStateToProps)(TodoPopup);