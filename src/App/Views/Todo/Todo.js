import React, { Component } from 'react';
import PostIt from '../../Elements/PostIt/PostIt'
import './todo.css'
import TodoPopup from './TodoPopup/TodoPopup'
import { connect } from 'react-redux'
import ChangeTodo from './ChangeTodo/ChangeTodo'

class Todo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            popupIsOpen: false,
            changeTodoPopupIsOpen: false,
            todoToChangeData: ''
        }

        this.handleClick = this.handleClick.bind(this);
        this.closePopup = this.closePopup.bind(this);
        this.handleChangePopup = this.handleChangePopup.bind(this);
        this.closeTodoChange = this.closeTodoChange.bind(this);
    }

    handleClick() {
        this.setState({
            popupIsOpen: this.state.popupIsOpen ? false : true
        });
    }

    closePopup() {
        this.setState({ popupIsOpen: false });
    }

    closeTodoChange() {
        this.setState({
            changeTodoPopupIsOpen: this.state.changeTodoPopupIsOpen ? false : true
        });
    }
    handleChangePopup(todo) {
        fetch('https://expressbacken.herokuapp.com/todos/single/' + todo + '')
            .then(res => res.json())
            .then(item => {
                this.setState({
                    todoToChangeData: item[0],
                    changeTodoPopupIsOpen: this.state.changeTodoPopupIsOpen ? false : true
                })
            })
            .catch(err => console.log(err))
    }

    render() {
        return (
            <div className='todo'>
                <div className='button-container'>
                    <button onClick={this.handleClick}>Ny lista</button>
                </div>
                {this.state.popupIsOpen ? <TodoPopup closeIt={this.closePopup} /> : ''}
                {this.state.changeTodoPopupIsOpen ? <ChangeTodo todoData={this.state.todoToChangeData} closeIt={this.closeTodoChange} /> : ''}
                {this.props.arr.map((data, key) => { console.log("asdasdddddd", data); return data.type != 'course' ? <PostIt handleChangePopup={this.handleChangePopup} postitID={data.id} key={key} todoTitle={data.title} todoID={data.id} todoItems={data.items} /> : '' })}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    arr: state.todos

});

export default connect(mapStateToProps)(Todo);
