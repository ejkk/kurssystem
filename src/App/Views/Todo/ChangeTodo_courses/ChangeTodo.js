import React, { Component } from 'react'
import './changetodo_c.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux'
import { updateTodo, newItems } from '../../../Redux/Actions/todos-actions'

class TodoPopup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputs: [],
            items: this.props.todoData.items,
            deleteItems: [],
            title: this.props.todoData.title,
        };

        this.newItem = this.newItem.bind(this);
        this.onTitleChange = this.onTitleChange.bind(this);
        this.onFirstItemChange = this.onFirstItemChange.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.updateTodo = this.updateTodo.bind(this);
        this.onItemChange = this.onItemChange.bind(this);
    }

    newItem() {
        var joined = this.state.inputs.concat('s' + this.state.inputs.length + 1);
        this.setState({ inputs: joined })
    }

    remove(id) {
        let element = document.getElementById(id);
        element.parentElement.innerHTML = '';
    }

    onTitleChange(e) {
        this.setState({
            title: this.state.title = e.target.value
        })
    }

    onFirstItemChange(e) {
        let items = this.state.items.slice();
        items[0].data = e.target.value
        this.setState({
            items
        })
    }

    onItemChange(e, index) {
        let items = this.state.items.slice();
        items[index].data = e.target.value
        this.setState({
            items
        })
    }

    deleteItem(index, id) {
        this.setState((prevState) => ({
            items: [...prevState.items.slice(0, index), ...prevState.items.slice(index + 1)]
        }))
        var joined = this.state.deleteItems.concat(id);
        this.setState({ deleteItems: joined }, () => {
        })
    }

    updateTodo() {
        var title = document.getElementsByName("title")[0].value;
        fetch('https://expressbacken.herokuapp.com/todos/title/update/' + this.props.todoData.id + '/' + title + '')

        this.state.items.map(item => {
            fetch('https://expressbacken.herokuapp.com/todos/item/update/' + item.id + '/' + item.data + '')
        })

        this.state.deleteItems.map(item => {
            fetch('https://expressbacken.herokuapp.com/todos/delete/item/' + item + '')
        })

        var items = document.getElementsByName("todItem");
        let i = 0;
        let item_values = [];
        for (i; i < this.state.inputs.length; i++) {
            item_values.push(items[i].value);
        }

        var newItem = item_values.map(item => {
            return { data: item, stroke: 0, id: Math.random().toString(36).substring(2, 14) + Math.random().toString(36).substring(2, 14), todos_id: this.props.todoData.id }
        })

        this.props.dispatch(newItems(newItem));

        newItem.map(item => {
            fetch('https://expressbacken.herokuapp.com/todos/items/new/' + item.data + '/' + this.props.todoData.id + '/' + item.id + '')

        })

        this.props.dispatch(updateTodo({
            title: this.state.title,
            id: this.props.todoData.id,
            items: this.state.items
        }));
    }

    style = {
        fontSize: '20px',
        marginBottom: '15px'
    }

    render() {
        let { inputs } = this.state;
        return (
            <div className='popup_c'>
                <div className='new-todo' id='test'>
                    <FontAwesomeIcon icon={faTimes} className='close-popup fa-m' onClick={this.props.closeIt} />
                    <div className='cont'>
                        <input type='text' name='title' placeholder='Titel' style={this.style} value={this.state.title} onChange={this.onTitleChange} />
                        <input type='text' placeholder='Att göra' value={this.state.items[0].data} onChange={this.onFirstItemChange} />
                        {this.state.items !== null && this.state.items.map((key, index) => {
                            let state = this.state
                            if (index !== 0) {
                                return this.state.items[index] !== null ? <div className='small-container' key={index} ><input value={state.items[index].data} onChange={(e) => this.onItemChange(e, index)} type='text' placeholder='Att göra' key={index} className='newInput' /><FontAwesomeIcon key={key + 3} icon={faTimes} className='icon fa-xs' onClick={(() => this.deleteItem(index, this.state.items[index].id))} /></div> : ''
                            }
                        })}
                        {inputs.map((key, index) => <div className='small-container' key={key + 1} ><input name='todItem' id={'input-' + index} type='text' placeholder='Att göra' key={key + 2} className='newInput' /><FontAwesomeIcon key={key + 3} icon={faTimes} className='icon fa-xs' onClick={(() => this.remove('input-' + index, this))} /></div>)}
                    </div>
                    <div className='btns'>
                        <button onClick={() => { this.newItem(); }}>Lägg till rad</button>
                        <button onClick={() => {
                            this.props.closeIt(); this.updateTodo();
                        }}>Spar</button>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    todosArr: state
});


export default connect(mapStateToProps)(TodoPopup);