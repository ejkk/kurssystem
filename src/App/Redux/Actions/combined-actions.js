export function newCourseNow(data) {
    return {
        type: 'newCourse',
        payload: data
    }
}