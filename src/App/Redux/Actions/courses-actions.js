export function deleteParticipant(data) {
    return {
        type: 'deleteParticipant',
        payload: data
    }
}

export function getAllCourses(data) {
    return {
        type: 'getAllCourses',
        payload: data
    }
}

export function updateCourse(data) {
    return {
        type: 'updateCourse',
        payload: data
    }
}