export function newTodo(data) {
    return {
        type: 'newTodo',
        payload: data
    }
}

export function loadAllTodos(data) {
    return {
        type: 'loadAllTodos',
        payload: data
    }
}

export function strokeItem(data) {
    return {
        type: 'strokeItem',
        payload: data
    }
}

export function deleteTodo(data) {
    return {
        type: 'deleteTodo',
        payload: data
    }
}

export function updateTodo(data) {
    return {
        type: 'updateTodo',
        payload: data
    }
}

export function newItems(data) {
    return {
        type: 'newItems',
        payload: data
    }
}