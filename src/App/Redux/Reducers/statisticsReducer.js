export default function statisticsReducer(rState = [], action) {
    switch (action.type) {
        case 'newCourse':
            let newState = {
                ...rState,
                activeCourses: rState.activeCourses + 1
            }
            return newState;
        default:
            return rState
    }
}