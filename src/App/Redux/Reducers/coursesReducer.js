export default function coursesReducer(rState = [], action) {
    switch (action.type) {
        case 'deleteParticipant':
            let partDeletedState = [
                ...rState.map(course => {
                    if (course.id === action.payload.courseid) {
                        return {
                            ...course,
                            participants: course.participants.filter(function (el) { return el.id !== action.payload.partID; })
                        }
                    } else return course
                })
            ]
            return partDeletedState

        case 'newCourse':
            return [...rState, action.payload]
        case 'getAllCourses':
            var newState = [...rState, ...action.payload]
            return newState;
        case 'updateCourse':
            let stateNew = rState.map(course => {
                if (course.id == action.payload.id) {
                    return {
                        ...course,
                        name: action.payload.name,
                        startDate: action.payload.startDate,
                        time: action.payload.time,
                        endTime: action.payload.endTime,
                        place: action.payload.place,
                        lunch: action.payload.lunch,
                        maxParticipants: action.payload.maxParticipants,
                        registrationEnds: action.payload.registrationCloses,
                        info: action.payload.info,
                        courseHasEnded: action.payload.courseHasEnded,
                        price: action.payload.price,
                    }
                } else {
                    return course
                }
            })
            return stateNew
        default:
            return rState
    }
}