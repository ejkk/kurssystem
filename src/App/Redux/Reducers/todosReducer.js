export default function todosReducer(rState = [], action) {
    switch (action.type) {
        case 'newTodo':
            var newState = [...rState, action.payload]
            return newState;
        case 'loadAllTodos':
            var newState1 = [...rState, ...action.payload]
            return newState1;
        case 'strokeItem':
            const todoObject = rState.map(todo => {
                if (todo.id === action.payload.postitPlace) {
                    return {
                        ...todo,
                        items: todo.items.map(item => {
                            if (item.id === action.payload.todoItemPlace) {
                                return {
                                    ...item,
                                    stroke: action.payload.strokeOn ? false : true
                                }
                            }
                            return item
                        })
                    }
                }
                return todo
            })
            return todoObject

        case 'newItems':
            // TODO: newItems
            return rState
        case 'updateTodo':
            let statenew = [
                ...rState.map(todo => {
                    if (todo.id === action.payload.id) {
                        return {
                            ...todo,
                            title: action.payload.title,
                            items: action.payload.items,
                            id: action.payload.id
                        }
                    } else return todo
                })
            ]

            return statenew
        case 'deleteTodo':
            let oos = [
                ...rState.filter(function (el) { return el.id !== action.payload.postitId; })
            ]
            return oos
        default:
            return rState
    }
}