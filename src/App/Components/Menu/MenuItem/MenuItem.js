import React, { Component } from 'react'
import './menuitem.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faListUl, faHome, faFileAlt, faChartBar } from '@fortawesome/free-solid-svg-icons'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

export default class MenuItem extends Component {
  render() {
    return (
      <div className='menu-item'>
        <Link to='/'><button className={this.props.path === '/' ? 'active' : ''}><FontAwesomeIcon icon={faHome} className='menu-icon' />Start</button></Link>
        <Link to='/courses'><button className={this.props.path === '/courses' ? 'active' : ''}><FontAwesomeIcon icon={faFileAlt} className='menu-icon' /> Kurser</button></Link>
        <Link to='/statistics'><button className={this.props.path === '/statistics' ? 'active' : ''}><FontAwesomeIcon icon={faChartBar} className='menu-icon' />Statistik</button></Link>
        <Link to='/todo'><button className={this.props.path === '/todo' ? 'active' : ''}><FontAwesomeIcon icon={faListUl} className='menu-icon' />Att göra</button></Link>
      </div>
    );
  }
}