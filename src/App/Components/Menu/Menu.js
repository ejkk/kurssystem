import React, { Component } from 'react';
import './menu.sass';
import MenuItem from './MenuItem/MenuItem'

export default class Manu extends Component {
  render() {
    return (
      <nav className='desktop-nav'>
        <MenuItem path={this.props.path} />
      </nav>
    );
  }
}