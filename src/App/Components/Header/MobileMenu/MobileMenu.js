import React, { Component } from 'react'
import './mobilemenu.css'
import MenuItem from '../../Menu/MenuItem/MenuItem'

export default class MobileMenu extends Component {
  style = {
    width: this.props.mobileMenuIsOpen ? '256px' : '0px'
  }

  render() {
    return (
      <nav className='mobile-nav' style={this.style}>
        <MenuItem path={this.props.path} />
      </nav>
    );
  }
}