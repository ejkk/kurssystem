import React, { Component } from 'react'
import './header.css'
import Banner from './Banner/Banner'
import ProfileInfo from './ProfileInfo/ProfileInfo'
import MobileMenu from './MobileMenu/MobileMenu'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'

export default class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mobileMenuIsOpen: false
    }

    this.changeMobileMenuStatus = this.changeMobileMenuStatus.bind(this);
  }

  changeMobileMenuStatus() {
    this.setState({
      mobileMenuIsOpen: this.state.mobileMenuIsOpen ? false : true
    });
  }

  render() {
    return (
      <header>
        <FontAwesomeIcon icon={faBars} className='m-menu-icon' onClick={this.changeMobileMenuStatus} />
        <Banner />
        <ProfileInfo />
        {this.state.mobileMenuIsOpen ? <MobileMenu path={this.props.match.path} mobileMenuIsOpen={this.state.mobileMenuIsOpen} /> : ''}
      </header>
    );
  }
}