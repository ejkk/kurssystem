import React, { Component } from 'react'
import './profileinfo.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown, faPowerOff } from '@fortawesome/free-solid-svg-icons'

export default class Header extends Component {
    constructor() {
        super();
        this.state = {
            dropIsOpen: false
        }
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        if (this.state.dropIsOpen === false) {
            this.setState({
                dropIsOpen: true
            });
        } else {
            this.setState({
                dropIsOpen: false

            });
        }
    }

    logOut() {
        document.cookie = 'session=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        window.location.replace('https://kurssystem.herokuapp.com/login.html')
    }

    render() {
        const profileImage = {
            backgroundImage: `url('http://pts-treuhand.ch/pics/personal/Kein%20Foto.png')`
        }

        return (
            <div className='profile-info'>
                <div className='mini-container' onClick={this.handleClick}>
                    <div className='profile-image' style={profileImage}></div>
                    <h5>Elin</h5>
                    <FontAwesomeIcon icon={faChevronDown} className='menu-icon fa-xs' />
                </div>
                <div className='drop-down'><button onClick={this.logOut} className={this.state.dropIsOpen ? 'logout open' : 'logout closed'}><FontAwesomeIcon icon={faPowerOff} className='fa-lg turn-off' /> Logga ut</button></div>
            </div>
        );
    }
}