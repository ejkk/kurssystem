import React, { Component } from 'react'
import './container.css'
import Main from '../Main/Main'
import Menu from '../Menu/Menu'

export default class Container extends Component {
  render() {
    return (
      <div className='container'>
        <Menu path={this.props.match.path} />
        <Main>
          {this.props.children}
        </Main>
      </div>
    );
  }
}