import React, { Component } from 'react'
import './main.css'

export default class Main extends Component {
  render() {
    return (
      <main>
        {this.props.children}
      </main>
    );
  }
}