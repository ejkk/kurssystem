import React, { Component } from 'react'
import './App.css'
import Header from '../Header/Header'
import Container from '../Container/Container'
import { BrowserRouter as Router, Route } from "react-router-dom";

import Courses from '../../Views/Courses/Courses'
import Statistics from '../../Views/Statistics/Statistics'
import Start from '../../Views/Start/Start'
import Todo from '../../Views/Todo/Todo'
import { loadAllTodos } from '../../Redux/Actions/todos-actions'
import { connect } from 'react-redux'


class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      data: '',
      aCourses: ''
    }

    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount() {
    /*var myCookie = this.getCookie("session");

    if (myCookie == null) {
      window.location.replace('https://kurssystem.herokuapp.com/login.html')
    }*/ //doesnt work with heroku

    var url_string = window.location.href
    var url = new URL(url_string);
    var c = url.searchParams.get("sh74h");

    if (c !== 'dkjsa73h') {
      window.location.replace("https://kurssystem.herokuapp.com/login.html");
    } else {

    }

    fetch('https://expressbacken.herokuapp.com/statistics')
      .then(res => res.json())
      .then(item => {
        this.setState({
          data: item
        })
      }).catch(err => console.log(err))

    fetch('https://expressbacken.herokuapp.com/statistics/courses')
      .then(res => res.json())
      .then(item => {
        this.setState({
          aCourses: item
        })
      }).catch(err => console.log(err))

    fetch('https://expressbacken.herokuapp.com/todos')
      .then(res => res.json())
      .then(item => {
        this.props.dispatch(loadAllTodos(item));
      }).catch(err => console.log(err))
  }

  render() {
    return (
      <div className="App">
        <Router>
          <Route path="/" exact render={(props) => <Header {...props} isAuthed={true}></Header>} />
          <Route path="/courses" render={(props) => <Header {...props} isAuthed={true}></Header>} />
          <Route path="/statistics" render={(props) => <Header {...props} isAuthed={true}></Header>} />
          <Route path="/todo" render={(props) => <Header {...props} isAuthed={true}></Header>} />

          <Route path="/" exact render={(props) => <Container {...props} isAuthed={true}><Start /></Container>} />
          <Route path="/courses" render={(props) => <Container {...props} isAuthed={true}><Courses /></Container>} />
          <Route path="/statistics" render={(props) => <Container  {...props} isAuthed={true}><Statistics sData={this.state.data} aCourses={this.state.aCourses} /></Container>} />
          <Route path="/todo" render={(props) => <Container {...props} isAuthed={true}><Todo /></Container>} />
        </Router>

      </div>
    );
  }
}

export default connect()(App);