import React, { Component } from 'react'
import './postit.css'
import { connect } from 'react-redux'
import PostItItem from './PostItItem/PostItItem'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCog } from '@fortawesome/free-solid-svg-icons'
import { deleteTodo } from '../../Redux/Actions/todos-actions'

class PostIt extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sDropdownIsOpen: false
    }

    this.handleClick = this.handleClick.bind(this)
    this.deleteTodo = this.deleteTodo.bind(this)
    this.handleChangeTodoStatus = this.handleChangeTodoStatus.bind(this)
  }

  handleClick() {
    this.setState({
      sDropdownIsOpen: this.state.sDropdownIsOpen ? false : true
    });
  }

  deleteTodo(e) {
    let postitID = e.target.id;
    this.props.dispatch(deleteTodo({ postitId: postitID }));
    fetch('https://expressbacken.herokuapp.com/todos/delete/' + postitID + '')
    this.setState({
      sDropdownIsOpen: this.state.sDropdownIsOpen ? false : true
    });
  }

  handleChangeTodoStatus(e) {
    this.props.handleChangePopup(e.target.id)
    this.setState({
      sDropdownIsOpen: this.state.sDropdownIsOpen ? false : true
    });
  }

  render() {
    return (
      <div className='post-it' style={this.state.sDropdownIsOpen ? { position: 'relative' } : { position: 'unset' }}>
        <FontAwesomeIcon icon={faCog} className='postit-settings' onClick={this.handleClick} type='input' />
        {this.state.sDropdownIsOpen ? <div className='settings-dropdown'><button className='change-btn' id={this.props.postitID} onClick={this.handleChangeTodoStatus} >Ändra</button><button className='delete-btn' id={this.props.postitID} onClick={this.deleteTodo} >Radera</button></div> : ''}
        <h3>{this.props.todoTitle}</h3>
        {this.props.todoItems.map(item => <PostItItem datas={item.data} stroke={item.stroke} postitID={this.props.postitID} postititemID={item.id} reduxState={this.props.arr} />)}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  arr: state.todos

});

export default connect(mapStateToProps)(PostIt);