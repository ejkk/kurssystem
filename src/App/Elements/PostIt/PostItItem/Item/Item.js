import React, { Component } from 'react'

export default class Item extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <div className='Item'><p>{this.props.items}</p></div>
        );
    }
}