import React, { Component } from 'react'
import { strokeItem } from '../../../Redux/Actions/todos-actions'

export default class PostItItem extends Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        let itemID = e.target.className
        fetch('https://expressbacken.herokuapp.com/todos/stroke/update/' + itemID + '/' + this.props.stroke + '')
        this.props.dispatch(strokeItem({ postitPlace: e.target.id, todoItemPlace: e.target.className, strokeOn: this.props.stroke }));
    }

    render() {
        return (
            <div className='postItitem item-border'>
                <p style={this.props.stroke ? { textDecoration: 'line-through', color: '#818181' } : { textDecoration: 'none', color: '#000000' }} id={this.props.postitID} className={this.props.postititemID} onClick={this.handleClick}>{this.props.datas}</p>
            </div>
        );
    }
}

