import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App/Components/App/App';
import * as serviceWorker from './serviceWorker';
import { combineReducers, createStore } from 'redux'
import { Provider } from 'react-redux'
import todosReducer from './App/Redux/Reducers/todosReducer'
import coursesReducer from './App/Redux/Reducers/coursesReducer'
import statisticsReducer from './App/Redux/Reducers/statisticsReducer'

const allReducers = combineReducers({
    todos: todosReducer,
    courses: coursesReducer,
    statistics: statisticsReducer
});

const store = createStore(allReducers, {
    todos: [],
    courses: [],
})

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

serviceWorker.unregister();




