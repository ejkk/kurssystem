var express = require('express');
const mysql = require('mysql')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
const app = express();

var mysqlConnection = mysql.createConnection({
  host: 'eu-cdbr-west-02.cleardb.net',
  user: 'bf52c4056b654b',
  password: '02fc8a0e',
  database: 'heroku_4eaad43f19c4b37',

})
app.use(cookieParser());

app.use(express.urlencoded())
app.get('/', (req, res) => {
  res.send('hej')
})
app.use(bodyParser.urlencoded({ extended: true }));
app.get('/todos', (req, res) => {
  var todos = []
  let todoitems = []

  mysqlConnection.query("SELECT * FROM todoitems", (err, data) => {
    if (!err) {
      for (let i = 0; i < data.length; i++) {
        todoitems.push({
          data: data[i].data,
          stroke: data[i].stroke,
          id: data[i].id,
          todos_id: data[i].todos_id,

        })
      }
    }
  })

  mysqlConnection.query("SELECT * FROM todos", (err, data) => {
    if (!err) {
      for (let i = 0; i < data.length; i++) {
        todos.push({
          title: data[i].title,
          items: [],
          id: data[i].id,
          type: data[i].type
        })
      }
      for (let i = 0; i < todos.length; i++) {
        for (let a = 0; a < todoitems.length; a++) {
          if (todos[i].id === todoitems[a].todos_id) {
            todos[i].items.push(todoitems[a])
          }
        }
      }
      if (todos.length !== 0) {
        res.setHeader('Access-Control-Allow-Origin', 'https://kurssystem.herokuapp.com');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', true);
        res.send(todos)
      }
      else {
        res.setHeader('Access-Control-Allow-Origin', 'https://kurssystem.herokuapp.com');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', true);
        res.end('stop')
      }
    }
  })
})

app.get('/todos/single/:postitID', (req, res) => {
  var todos = []
  let todoitems = []

  mysqlConnection.query("SELECT * FROM todoitems WHERE ?", { todos_id: req.params.postitID }, (err, data) => {
    if (!err) {
      for (let i = 0; i < data.length; i++) {
        todoitems.push({
          data: data[i].data,
          stroke: data[i].stroke,
          id: data[i].id,
          todos_id: data[i].todos_id
        })
      }
    }
  })

  mysqlConnection.query("SELECT * FROM todos WHERE ?", { id: req.params.postitID }, (err, data) => {
    if (!err) {
      for (let i = 0; i < data.length; i++) {
        todos.push({
          title: data[i].title,
          items: [],
          id: data[i].id,
        })
      }
      for (let i = 0; i < todos.length; i++) {
        for (let a = 0; a < todoitems.length; a++) {
          if (todos[i].id === todoitems[a].todos_id) {
            todos[i].items.push(todoitems[a])
          }
        }
      }
      if (todos.length !== 0) {
        res.setHeader('Access-Control-Allow-Origin', 'https://kurssystem.herokuapp.com');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', true);
        res.send(todos)
      }
      else {
        res.setHeader('Access-Control-Allow-Origin', 'https://kurssystem.herokuapp.com');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', true);
        res.end('stop')
      }
    }
  })
})

app.get('/todos/courseTodo/:courseID', (req, res) => {
  var todos = []
  let todoitems = []

  mysqlConnection.query("SELECT * FROM todoitems WHERE ?", { todos_id: req.params.courseID }, (err, data) => {
    if (!err) {
      for (let i = 0; i < data.length; i++) {
        todoitems.push({
          data: data[i].data,
          stroke: data[i].stroke,
          id: data[i].id,
          todos_id: data[i].todos_id
        })
      }
    }
  })

  mysqlConnection.query("SELECT * FROM todos WHERE ?", { id: req.params.courseID }, (err, data) => {
    if (!err) {
      for (let i = 0; i < data.length; i++) {
        todos.push({
          title: data[i].title,
          items: [],
          id: data[i].id,
        })
      }
      for (let i = 0; i < todos.length; i++) {
        for (let a = 0; a < todoitems.length; a++) {
          if (todos[i].id === todoitems[a].todos_id) {
            todos[i].items.push(todoitems[a])
          }
        }
      }
      if (todos.length !== 0) {
        res.setHeader('Access-Control-Allow-Origin', 'https://kurssystem.herokuapp.com');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', true);
        res.send(todos)
      }
      else {
        res.setHeader('Access-Control-Allow-Origin', 'https://kurssystem.herokuapp.com');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', true);
        res.end('stop')
      }
    }
  })
})

app.get('/todos/stroke/update/:itemID/:stroke', (req, res) => {
  let strokeValue = req.params.stroke;
  let newStrokeValue;

  if (strokeValue == 1) {
    newStrokeValue = 0;
  } else if (strokeValue == 0) {
    newStrokeValue = 1;
  }
  mysqlConnection.query('UPDATE todoitems SET stroke = ? WHERE id = ?', [newStrokeValue, req.params.itemID], (err, data) => {
    res.send(err)
  })
})

app.get('/todos/title/update/:todoID/:atitle', (req, res) => {
  let title = req.params.atitle;
  let todoID = req.params.todoID;

  mysqlConnection.query('UPDATE todos SET ? WHERE ?', [{ title: title }, { id: todoID }], (err, data) => {
    res.send(err)
  })
})

app.get('/todos/item/update/:itemID/:data', (req, res) => {

  mysqlConnection.query('UPDATE todoitems SET data = ? WHERE id = ?', [req.params.data, req.params.itemID], (err, data) => {
    res.send(err)
  })
})

app.get('/todos/delete/item/:itemID', (req, res) => {
  mysqlConnection.query("DELETE FROM todoitems WHERE ?", { id: req.params.itemID }, (err, data) => {
    res.send(err)
  })
})


app.get('/todos/new/:title/:id', (req, res) => {
  mysqlConnection.query("INSERT INTO todos SET ?", { title: req.params.title, id: req.params.id, type: '', owned_by_used_id: '', courseID: '' }, (err) => {
    res.send(err)
  })

})

app.get('/todos/items/new/:data/:todoID/:id', (req, res) => {
  mysqlConnection.query("INSERT INTO todoitems SET ?", { todos_id: req.params.todoID, data: req.params.data, id: req.params.id, stroke: 0 }, (err) => {
    res.send(err)
  })
})


app.get('/todos/delete/:id', (req, res) => {
  mysqlConnection.query("DELETE FROM todos WHERE ?", { id: req.params.id })
})

app.get('/courses', (req, res) => {
  let allCourses = []
  mysqlConnection.query("SELECT * FROM courses", (err, data) => {
    if (!err) {
      for (let i = 0; i < data.length; i++) {
        allCourses.push({
          name: data[i].name, startDate: data[i].start_date, time: data[i].start_time, place: data[i].place, lunch: data[i].lunch, maxParticipants: data[i].max_participants, amountOfCurrentparticipants: data[i].amount_of_current_participants, registrationEnds: data[i].registration_ends, info: data[i].info, courseHasEnded: data[i].course_has_ended, id: data[i].id, price: data[i].price, endTime: data[i].end_time, participants: []
        })
      }
    }
  })

  mysqlConnection.query("SELECT * FROM courseparticipants", (err, data) => {
    for (let i = 0; i < allCourses.length; i++) {
      for (let a = 0; a < data.length; a++) {
        if (allCourses[i].id === data[a].course_id) {
          allCourses[i].participants.push({
            foreName: data[a].foreName,
            sureName: data[a].sureName,
            needsHotel: data[a].needsHotel,
            registered: data[a].registered,
            id: data[a].id,
            course_id: data[a].course_id,
          })
        }
      }
    }
    if (allCourses.length !== undefined) {
      res.setHeader('Access-Control-Allow-Origin', 'https://kurssystem.herokuapp.com');
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
      res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
      res.setHeader('Access-Control-Allow-Credentials', true);
      res.send(allCourses)
    }
    else {
      res.setHeader('Access-Control-Allow-Origin', 'https://kurssystem.herokuapp.com');
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
      res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
      res.setHeader('Access-Control-Allow-Credentials', true);
      res.end('stop')
    }
  })

})

app.get('/courses/new/:name/:startDate/:startTime/:endTime/:price/:place/:maxParticipants/:registrationEnds/:info/:id/:lunch', (req, res) => {

  let newCourse = {
    name: req.params.name, startDate: req.params.startDate,
    time: req.params.startTime, endTime: req.params.endTime, place: req.params.place,
    lunch: req.params.lunch,
    maxParticipants: req.params.maxParticipants,
    amountOfCurrentParticpipants: 0,
    registrationEnds: req.params.registrationEnds,
    info: req.params.info,
    courseHasEnded: 0,
    id: req.params.id,
    price: req.params.price,
  }
  mysqlConnection.query("INSERT INTO courses SET ?", {
    name: newCourse.name, start_date: newCourse.startDate, start_time: newCourse.time, end_time: newCourse.endTime, place: newCourse.place, lunch: newCourse.lunch, max_participants: newCourse.maxParticipants, amount_of_current_participants: newCourse.amountOfCurrentParticpipants, registration_ends: newCourse.registrationEnds, info: newCourse.info, course_has_ended: 0,
    id: newCourse.id,
    price: newCourse.price,
  }, (err) => {

    mysqlConnection.query("INSERT INTO todos SET ?", { title: req.params.name, id: req.params.id, type: 'course' }, (err) => {

    })
    mysqlConnection.query("INSERT INTO todoitems SET ?", { data: 'Att göra', id: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15), todos_id: req.params.id, stroke: 0 }, (err) => {

    })
    res.setHeader('Access-Control-Allow-Origin', 'https://kurssystem.herokuapp.com');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.send(err)
  })
})

app.get('/courses/update/:name/:startDate/:startTime/:endTime/:price/:place/:maxParticipants/:registrationEnds/:info/:id/:lunch', (req, res) => {
  let newCourse = {
    name: req.params.name,
    startDate: req.params.startDate,
    time: req.params.startTime,
    endTime: req.params.endTime,
    place: req.params.place,
    lunch: req.params.lunch,
    maxParticipants: req.params.maxParticipants,
    amountOfCurrentParticpipants: 0,
    registrationEnds: req.params.registrationEnds,
    info: req.params.info,
    courseHasEnded: false,
    id: req.params.id,
    price: req.params.price,
  }
  mysqlConnection.query("UPDATE courses SET ? WHERE ?", [{
    name: newCourse.name,
    start_date: newCourse.startDate,
    start_time: newCourse.time,
    end_time: newCourse.endTime,
    place: newCourse.place,
    lunch: newCourse.lunch,
    max_participants: newCourse.maxParticipants,
    amount_of_current_participants: newCourse.amountOfCurrentParticpipants,
    registration_ends: newCourse.registrationEnds,
    info: newCourse.info,
    course_has_ended: false,
    price: newCourse.price,
  }, { id: req.params.id }], (err) => {
    res.send(err)
  })

})

app.get('/courses/participant/registration/:foreName/:sureName/:courseID/:price', (req, res) => {
  var date = new Date();
  var today = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();

  mysqlConnection.query("INSERT INTO courseparticipants SET ?", {
    foreName: req.params.foreName,
    sureName: req.params.sureName,
    needsHotel: false,
    registered: today,
    id: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15),
    course_id: req.params.courseID
  }, () => {
    mysqlConnection.query("SELECT * FROM statistics", (err, data) => {
      var cashMade = data[0].cashMade
      var cash = cashMade + parseInt(req.params.price)
      var total_participants = data[0].total_participants
      var totalParticipants = total_participants + 1
      mysqlConnection.query("UPDATE statistics SET ?", { cashMade: cash, total_participants: totalParticipants }, (err) => {
        res.send(err)
      })
    }, () => {

    })
    //res.redirect('/statistics/cashMade/add/'+req.params.price+'')
  })

})

app.get('/courses/participant/delete/:id', (req, res) => {
  mysqlConnection.query("DELETE FROM courseparticipants WHERE ?", { id: req.params.id }, (err) => {
    res.send(err)
  })
})

app.get('/statistics', (req, res) => {
  var allStatistics;
  mysqlConnection.query("SELECT * FROM statistics", (err, data) => {

    allStatistics = {
      totalParticipants: data[0].total_participants,
      cashMade: data[0].cashMade
    }
    res.setHeader('Access-Control-Allow-Origin', 'https://kurssystem.herokuapp.com');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.send(allStatistics)
  })

})

app.get('/statistics/courses', (req, res) => {

  mysqlConnection.query("SELECT COUNT(*) AS amountOfCourses FROM courses", (err, data) => {
    res.setHeader('Access-Control-Allow-Origin', 'https://kurssystem.herokuapp.com');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.send(data[0])
  })

})

app.get('/statistics/cashMade/', (req, res) => {

  mysqlConnection.query("SELECT * FROM statistics", (err, data) => {
    res.send(data[0].cashMade)
  })

})

app.post('/login', (req, res) => {
  if (req.body.username === 'Elin' && req.body.password === 'elin') {
    //res.cookie("session", "user"); does not work on heroku

    res.setHeader('Access-Control-Allow-Origin', 'https://kurssystem.herokuapp.com');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.redirect('https://kurssystem.herokuapp.com?sh74h=dkjsa73h');
  }
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`App runs on port: ${PORT}`);
});